module.exports = {
    // 网站标题
    title: '企慧宝FAQ',
    // 网站描述
    description: '企慧宝FAQ with vitePress',
    // 打包目录
    dest: './dist',
    base:'/qhb-faq/',
    // 头部head
    head: [
        // 添加图标
        ['link', { rel: 'icon', href: '/favicon.ico' }]
    ],
    // 使用插件
    plugins: [
        '@vuepress/active-header-links',
        '@vuepress/back-to-top',
        '@vuepress/medium-zoom',
        '@vuepress/nprogress',
      [
        '@vuepress/plugin-search',
        {
          locales: {
            '/': {
              placeholder: 'Search',
            },
            '/zh/': {
              placeholder: '搜索',
            },
          },
        },
      ],
    ],
    // 主题配置
    themeConfig: {
        // 获取每个文件最后一次 git 提交的 UNIX 时间戳(ms)，同时它将以合适的日期格式显示在每一页的底部
        // lastUpdated: 'Last Updated', // string | boolean
        // 启动页面丝滑滚动
        smoothScroll: true,
        // 导航栏配置
        nav:[
            {text: '企慧宝后管系统', link: 'http://123.124.21.115:8081/admin-cloud/' },
            {text: '天地融科技官网', link: 'http://www.tendyron.com/'},
        ],
        sidebar:{
            '/':getSidebar()
        }
    }
}

function getSidebar() {
    return [
        {
            text:'FAQ',
            children: [
                { text: '问题分类', link: '/FAQ/' },
            ],
            sidebarDepth:3
        },
    ]
}
