---
title 基础
---

## Q1:设备显示“离线状态”


![图片](/Q1.jpg)

**解决方案**：网络问题，排查网络故障后重启应用。

## Q2:设备提示“未到开餐时间”

![图片](/Q2.jpg)

**解决方案**：设备当前时间不在餐别开餐时间内，进入设备`【系统设置】`修改设备时间该餐别对应的`【用餐时间】`后，重启应用。

## Q3:应用更新时提示“下载出错”

![图片](/Q3.jpg)

**解决方案**：直接点击`【继续更新】`继续下载；如果进度条一直没变化，检查网络是否正常，重启应用继续下载。


## Q4:设备提示“请启动或重启主机设备”

![图片](/Q4.jpg)

**解决方案**：检查设备关联的主机是否处于非在线状态（包含但不限于关机、断网、进入设置页面等飞正常使用状态），重启主机使主机进入在线状态。

## Q5:设备提示“不支持现场取餐”

![图片](/Q5.jpg)

**解决方案**：进入后管`【智慧餐厅】-【餐厅管理】-【餐别管理】`，对应餐别的`【业务开关】`选勾`“现场”`，重启应用。

## Q6:设备提示“未到堂食现场取餐时间”

![图片](/Q6.jpg)

**解决方案**：设备当前时间不在餐别开餐时间内，进入设备`【系统设置】`修改设备时间该餐别对应的`【现场用餐时间】`后，重启应用。

## Q7:设备提示“人员（餐票）类型：XXX未设置收费标准”

![图片](/Q7.jpg)

**解决方案**：进入后管`【智慧餐厅】-【餐厅管理】-【餐别管理】`，对应餐别的`【收费标准】-【新增人员类型标准】`添加对应人员类型的收费标准后，重启应用。

## Q8:设备提示“套餐：XXX[现场]-个人取餐份数超过限制（X份）”

![图片](/Q8.jpg)

**解决方案**：超过了套餐的个人现场取餐份数限制，进入后管`【智慧餐厅】-【餐厅管理】-【餐别管理】`，对应餐别修改相应套餐的`【现场】`份数限制，重启应用。

## Q9:设备提示“套餐：XXX[订餐+现场]-个人总份数超过限制（5份）”

![图片](/Q9.jpg)

**解决方案**：超过了套餐的个人取餐总份数限制，进入后管`【智慧餐厅】-【餐厅管理】-【餐别管理】`，对应餐别修改相应套餐的`【订餐+现场】`份数限制，重启应用。

## Q10:设备提示“配菜不存在。请检查配菜数据”

![图片](/Q10.jpg)

**解决方案**：没有设置配菜信息，进入后管`【智慧餐厅】-【菜品管理】-【配菜信息】`，添加对应餐别的配菜信息，重启应用。
