---
home: true
// heroImage: /favicon.ico
heroAlt: Logo image
heroText: 企慧宝FAQ
tagline: 查看企慧宝FAQ解决您所遇到的问题
actionText: 查看FAQ文档
actionLink: /FAQ/
features:
  - title: 解决方案
    details: 解决方案文案...。
  - title: 应用更新
    details: 应用更新文案...。
  - title: 设备提示
    details: 设备提示文案...。
# footer: MIT Licensed | Copyright © 2021-present CoolDream
---
